package com.serene.tests.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BasePage {

	public void waitForEmailToBeReceived(int seconds) {
		try {
			System.out.println("----Waiting for email to receive !!");
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			System.out.println("----Error while waiting for " + seconds + " seconds.");
		}
	}
	
	public String getCurrentDateAndTime() throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(new Date());
	}
}
