package com.serene.tests.constants;

public class EmailConstants {

	
	public static final String SMTPS_EMAIL_PROTOCOL_VALUE = "smtps";
	public static final String IMAP_EMAIL_PROTOCOL_VALUE = "imap";
	public static final String IMAP_EMAIL_PROTOCOL_KEY = "mail.store.protocol";
	public static final String SMTP_HOST_KEY = "mail.smtp.host";
	public static final String SMTP_HOST_VALUE = "smtp.gmail.com";
	public static final String SMTP_USER = "mail.smtp.user";
	public static final String SMTP_PORT_KEY = "mail.smtp.port";
	public static final String SMTP_PORT_VALUE = "465";
	public static final String SMTP_STARTTLS_ENABLE= "mail.smtp.starttls.enable";
	public static final String SMTP_DEBUG = "mail.smtp.debug";
	public static final String SMTP_AUTH = "mail.smtp.auth";
	public static final String SMTP_SCOKET_FACTORY_KEY ="mail.smtp.socketFactory.class";
	public static final String SMTP_SCOKET_FACTORY_VALUE ="javax.net.ssl.SSLSocketFactory";
	public static final String SMTP_SOCKET_FACTORY_FALLBACK = "mail.smtp.socketFactory.fallback";
	public static final String IMAPS_PARTIALFETCH = "mail.imaps.partialfetch";
	public static final String IMAP_SSL_ENABLE = "mail.imap.ssl.enable";
	public static final String MIME_BASE64_IGNOREERRORS = "mail.mime.base64.ignoreerrors";
	public static final String IMAP_STRTTLS_ENABLE = "mail.imap.starttls.enable";
	public static final String IMAP_HOST_VALUE = "imap.gmail.com";
	public static final String INBOX_FOLDER = "INBOX";
	public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String MEME_TYPE_TEXT_PLAIN= "text/plain";
	public static final String MEME_TYPE_TEXT_HTML= "text/html";
	public static final String SENDER_USERNAME = "SENDER_USERNAME";
	public static final String SENDER_EMAIL_PASSWORD = "SENDER_EMAIL_PASSWORD";
	public static final String RECEIVER_EMAIL_PASSWORD = "RECEIVER_EMAIL_PASSWORD";
	public static final Integer IMAP_PORT_VALUE = 993;
	public static final String EMAIL_SUBJECT = "emailSubject";
	public static final String EMAIL_MESSAGE = "emailMessage";
}
