package com.serene.tests.features;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = { "classpath:features" },
		glue = {"classpath:com/serene/tests/steps", "classpath:com/serene/tests/utilities"},
		tags = {},
		strict = true// will fail execution if there are undefined or pending steps
		, monochrome = true) // display console output in a readable format
public class TestRunner {

}
