package com.serene.tests.steps;

import java.text.ParseException;
import java.util.List;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.junit.runner.RunWith;

import com.serene.tests.utilities.EmailApiUtility;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityRunner;

@RunWith(SerenityRunner.class)
public class EmailStepDefination {

	public String emailContent;
	public EmailApiUtility emailApiUtility = new EmailApiUtility();
	private Session session;
	private MimeMessage mimemessage;

	@Before
	public void setup() {
		RestAssured.baseURI = "https://gmail.com";

	}

	@After
	public void tearDown() {
		RestAssured.reset();
	}

	@Given("^User get login credentials from system$")
	public void user_is_able_to_get_login_credentials() throws ParseException {
		emailApiUtility.getCredentials();
	}
	
	@And("^User log in to Gmail application SMTP Session to send Email using \"([^\"]*)\" Email address$")
	public void user_is_able_to_Login_to_gmail(String senderEmail) throws ParseException {
		session = emailApiUtility.gmailLogin(senderEmail);
	}
	
	@Then ("^Verify if error check returns false for user login$")
	public void verify_error_check_for_login() {
		emailApiUtility.checkSuccessfulLogin();
	}

	@When("^User draft the email to send using below email details$")
	public void user_drafts_email(List<List<String>> emailDetails) {
		String senderEmail = emailDetails.get(1).get(0);
		String receiverEmail = emailDetails.get(1).get(1);
		String subject = emailDetails.get(1).get(2);
		String emailMessage = emailDetails.get(1).get(3);
		mimemessage = emailApiUtility.draftEmail(senderEmail, receiverEmail, subject, emailMessage);
	}

	@Then ("Verify if error check returns false for email draft")
	public void verify_error_check_for_email_draft() {
		emailApiUtility.checkSuccessfulEamilDraft();
	}
	
	@When("^User sends the email$")
	public void user_is_able_to_send_email() {
		emailApiUtility.sendEmail(session, mimemessage);
	}

	@Then("^Verify that a newly received email has correctly displayed with below details$")
	public void user_reads_and_verifies_the_email_content(List<List<String>> emailDetails) throws Throwable {
		String receiverEmail = emailDetails.get(1).get(0);
		String senderEmail = emailDetails.get(1).get(1);
		String searchCriteria = emailDetails.get(1).get(2);
		String searchString = emailDetails.get(1).get(3);
		String emailMessage = emailDetails.get(1).get(4);

		emailApiUtility.verifyMailToken(receiverEmail, senderEmail, searchCriteria, searchString, emailMessage);
	}

	
}
