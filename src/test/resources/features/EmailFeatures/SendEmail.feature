Feature: Send and verify Received Email


  Scenario: Verify USer is able to login and able to send an email without attachment and then verify that it is recieved.
    Given User get login credentials from system
    And User log in to Gmail application SMTP Session to send Email using "leaseplansender@gmail.com" Email address
    Then Verify if error check returns false for user login 
    When User draft the email to send using below email details
      | SenderEmail               | ReceiverEmail               | subject         | emailMessage       |
      | leaseplansender@gmail.com | leaseplanreceiver@gmail.com | test send email | This is test email |
    Then Verify if error check returns false for email draft
    When User sends the email
    Then Verify that a newly received email has correctly displayed with below details
      | ReceiverEmail               | SenderEmail               | SearchCriteria | SearchString    | emailMessage       |
      | leaseplanreceiver@gmail.com | leaseplansender@gmail.com | subject        | test send email | This is test email |
    
#Scenario: Verify USer is able to login and able to send an email with attachment and then verify that it is recieved.

#  Scenario: Verify that a newly received email is displayed as highlighted in the Inbox section.

#  Scenario: Verify that a newly received email has correctly displayed sender email Id or name, mail subject and mail body(trimmed to a single line).

#  Scenario: Verify that the email contents are correctly displayed with the desired source formatting.

#  Scenario: Verify that any attachments are attached to the email and are downloadable.

# Scenario: Verify that all the emails marked as read are not highlighted.

# Scenario: Verify that all the emails read as well as unread have a mail read time appended at the end on the email list displayed in the inbox section.

# Scenario: Verify that count of unread emails is displayed alongside ‘Inbox’ text in the left sidebar of Gmail.

# Scenario: Verify that unread email count increases by one on receiving a new email.

# Scenario: Verify that unread email count decreases by one on reading an email ( marking an email as read).

# Scenario: Verify that email recipients in cc are visible to all users.

# Scenario: Verify that email recipients in bcc are not visible to the user.

#  Scenario: Verify that email can be received from non-Gmail email Ids like – yahoo, Hotmail etc.
  
  
